import { commonMessage } from "../constants/messageConstants";

export function noRecordsReplacer() {
  const emptyText = <span> {commonMessage.EMPTY_TEXT} </span>;

  return { emptyText };
}
