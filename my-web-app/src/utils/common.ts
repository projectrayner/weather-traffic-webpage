import { Float } from "react-native/Libraries/Types/CodegenTypes";

export function findNearestLocation(
  lat: string,
  long: string,
  listOfLocations: Array<any>
) {
  const list = getListOfDistanceBetweenTwoLocations(
    parseFloat(lat),
    parseFloat(long),
    listOfLocations
  );
  const nearestLocObj = list.reduce(
    (minObject, currentObject) =>
      currentObject.dist < minObject.dist ? currentObject : minObject,
    list[0]
  );
  return nearestLocObj.locationName;
}

function getListOfDistanceBetweenTwoLocations(
  lat: Float,
  long: Float,
  listOfLocations: Array<any>
) {
  return listOfLocations.map((loc: any) => {
    return {
      locationName: loc.location,
      dist: calculateDistance(lat, long, loc.lat, loc.long),
    };
  });
}

export function calculateDistance(
  lat1: number,
  lon1: number,
  lat2: number,
  lon2: number
) {
  if (lat1 === lat2 && lon1 === lon2) {
    return 0;
  } else {
    var radlat1 = (Math.PI * lat1) / 180;
    var radlat2 = (Math.PI * lat2) / 180;
    var theta = lon1 - lon2;
    var radtheta = (Math.PI * theta) / 180;
    var dist =
      Math.sin(radlat1) * Math.sin(radlat2) +
      Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
    if (dist > 1) {
      dist = 1;
    }
    dist = Math.acos(dist);
    dist = (dist * 180) / Math.PI;
    dist = dist * 60 * 1.1515;
    dist = dist * 1.609344; // in km
    return dist;
  }
}
