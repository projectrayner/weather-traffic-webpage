import { Image, Row, Space } from "antd";

export function TrafficImage(props: any) {
  const { selectedItem } = props;
  return (
    <Space direction="vertical">
      <Image height={300} src={selectedItem.image} />
    </Space>
  );
}
