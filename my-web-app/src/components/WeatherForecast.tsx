import { Image, Space, Typography } from "antd";
import weather from "../assets/images/weather.png";

const { Title } = Typography;

export function WeatherForecast(props: any) {
  const { selectedItem } = props;
  return (
    <Space direction="vertical">
      <Image width={150} src={weather} preview={false} />
      <Title level={3} style={{ margin: 0 }}>
        {selectedItem.weather}
      </Title>
    </Space>
  );
}
