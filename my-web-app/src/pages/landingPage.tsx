import {
  Button,
  Card,
  DatePicker,
  Input,
  List,
  Row,
  TimePicker,
  Typography,
} from "antd";

import { Content } from "antd/es/layout/layout";
import { useEffect, useRef, useState } from "react";
import { getTrafficImagesAPI, getWeather2HoursAPI } from "../apis/apiFunctions";
import { TrafficImage } from "../components/TrafficImage";
import { WeatherForecast } from "../components/WeatherForecast";
import { findNearestLocation } from "../utils/common";
import { noRecordsReplacer } from "../utils/valueReplacer";

export default function LandingPage() {
  const [dateTime, setDateTime] = useState({} as any);

  const [trafficImages, setTrafficImages] = useState([]);
  const [listOfLocations, setListOfLocations] = useState([] as any);
  const [listOfWeatherForecast, setListOfWeatherForecast] = useState([] as any);

  const [loading, setLoading] = useState(false);
  const [selectedItem, setSelectedItem] = useState({} as any);
  const [hasError, setHasError] = useState(false);

  const hasDateAndTime = Object.keys(dateTime).length === 2;
  const hasLocations = listOfLocations.length !== 0;
  const hasSelectedItem = Object.keys(selectedItem).length !== 0;
  const hasTrafficImages = trafficImages.length !== 0;

  const { date, time } = dateTime;
  useEffect(() => {
    hasDateAndTime && getListOfLocations(date, time);
  }, [dateTime]);

  useEffect(() => {
    hasLocations && getTrafficImages(date, time);
  }, [listOfLocations]);

  async function getTrafficImages(date: string, time: string) {
    setLoading(true);

    const dateTimeParams = `${date}T${time}`;
    try {
      const res = await getTrafficImagesAPI(dateTimeParams);
      const data = res.data.items[0].cameras;
      const mapped = data.map((item: any) => {
        const { location, camera_id, image } = item;
        const { latitude, longitude } = location;
        return {
          camera_id: camera_id,
          location: findNearestLocation(latitude, longitude, listOfLocations),
          lat: latitude,
          long: longitude,
          image: image,
        };
      });
      setTrafficImages(mapped);
    } catch (e) {
      setHasError(true);
    } finally {
      setLoading(false);
    }
  }

  async function getListOfLocations(date: string, time: string) {
    try {
      const dateTime = `${date}T${time}`;
      const res = await getWeather2HoursAPI(dateTime);
      const locationsWithLatLong = res.data.area_metadata;
      const weatherForecasts = res.data.items[0].forecasts;
      const mapped = locationsWithLatLong.map((item: any) => {
        const { label_location, name } = item;
        return {
          lat: label_location.latitude,
          long: label_location.longitude,
          location: name,
        };
      });
      setListOfLocations(mapped);
      setListOfWeatherForecast(weatherForecasts);
    } catch (e) {
      setHasError(true);
    } finally {
      setLoading(false);
    }
  }

  const handleDateTimeChange = (value: any, type: string) => {
    let selectedDateTime = { ...dateTime };
    if (type === "date") {
      selectedDateTime[type] = value.format("YYYY-MM-DD");
    } else if (type === "time") {
      selectedDateTime[type] = value.format("HH:mm:ss");
    }
    setDateTime(selectedDateTime);
  };

  const handleClick = (selected: any) => {
    const found: any = trafficImages.find(
      (item: any) => item.camera_id === selected.camera_id
    );
    const found2: any = listOfWeatherForecast.find(
      (item: any) => item.area === selected.location
    );
    const itemSelected = {
      image: found.image,
      weather: found2.forecast,
    };
    setSelectedItem(itemSelected);
  };

  return (
    <Content className="site-layout-content">
      <Card className="site-content-card">
        <Row className="container-picker">
          <DatePicker
            className="picker"
            onChange={(value) => {
              handleDateTimeChange(value, "date");
            }}
          />
          <TimePicker
            className="picker"
            onChange={(value) => {
              handleDateTimeChange(value, "time");
            }}
          />
        </Row>
        <Row className={`container ${!hasSelectedItem && "display-list-only"}`}>
          {hasTrafficImages && (
            <div className="component location-list">
              <List
                locale={noRecordsReplacer()}
                loading={loading}
                itemLayout="horizontal"
                dataSource={trafficImages}
                pagination={{
                  pageSize: 4,
                  showSizeChanger: false,
                }}
                renderItem={(item: any, index) => (
                  <List.Item
                    actions={[
                      <Button type="primary" onClick={() => handleClick(item)}>
                        View
                      </Button>,
                    ]}
                  >
                    <List.Item.Meta
                      title={item.location}
                      description={`Latitude: ${item.lat}, Longitude: ${item.long}`}
                    />
                  </List.Item>
                )}
              />
            </div>
          )}
          {hasSelectedItem && (
            <>
              <div className="component weather-image">
                <WeatherForecast selectedItem={selectedItem} />
              </div>
              <div className="component traffic-image">
                <TrafficImage selectedItem={selectedItem} />
              </div>
            </>
          )}
        </Row>
      </Card>
    </Content>
  );
}
