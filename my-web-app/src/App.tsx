import "./App.css";

import LandingPage from "./pages/landingPage";

function App() {
  return (
    <div className="App-header">
      <LandingPage />
    </div>
  );
}

export default App;
