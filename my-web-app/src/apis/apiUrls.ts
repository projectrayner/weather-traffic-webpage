// const env: string = process.env.NODE_ENV;
const env: string = "LOCAL";
const baseUrl: string = "http://localhost:3003/api";
const version: string = "/v1";

export const apiUrl = {
  weather2hoursUrl: baseUrl + version + "/weather-2hours",
  trafficImagesUrl: baseUrl + version + "/traffic-images",
};
