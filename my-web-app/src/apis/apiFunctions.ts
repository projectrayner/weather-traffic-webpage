import axios from "axios";
import { apiUrl as api } from "./apiUrls";

export function getTrafficImagesAPI(dateTime?: string) {
  const url = api.trafficImagesUrl + `?date_time=${dateTime}`;
  return axios.get(url);
}

export function getWeather2HoursAPI(dateTime?: string) {
  const withParams = dateTime ? `?date_time=${dateTime}` : "";
  const url = api.weather2hoursUrl + withParams;
  return axios.get(url);
}
