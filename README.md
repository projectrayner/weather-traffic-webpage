# Weather Traffic Webpage

## Name

Traffic-Weather Forecast Appplication

## Description

A mini project

## Installation

1. Ensure that you have the latest version of Node (https://nodejs.org/en) and Git (https://git-scm.com/downloads)
2. Navigate to the directory that you want to clone the project into.
3. Launch Command Prompt window
4. Clone the project from https://gitlab.com/projectrayner/weather-traffic-webpage

```
git clone https://gitlab.com/projectrayner/weather-traffic-webpage.git
```

5. Change directory to the client folder

```
cd weather-traffic-webpage/my-web-app
```

6. Launch another Command Prompt window for the server
7. Navigate to the directory that the project resides.

```
cd weather-traffic-webpage/weather-traffic-server
```

8. Install node modules for both client and server

```
npm install
```

9. Once installation has completed, start both client and server

```
npm start
```

10. The web application will automatically opens in the browser with http://localhost:3000/
