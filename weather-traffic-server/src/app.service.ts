import { HttpService } from '@nestjs/axios';
import { ForbiddenException, Injectable } from '@nestjs/common';
import { catchError, map } from 'rxjs';

// https://data.gov.sg/dataset/traffic-images
@Injectable()
export class AppService {
  constructor(private httpService: HttpService) {}

  async getImages(date_time: string) {
    const params = date_time ? `?date_time=${date_time}` : '';
    return this.httpService
      .get(`https://api.data.gov.sg/v1/transport/traffic-images` + params)
      .pipe(
        map((res) => {
          return res.data;
        }),
      )
      .pipe(
        catchError(() => {
          throw new ForbiddenException('Something went wrong');
        }),
      );
  }

  async getWeather2Hour(date_time: string) {
    // /environment/24-hour-weather-forecast
    // /environment/4-day-weather-forecast
    const params = date_time ? `?date_time=${date_time}` : '';
    return this.httpService
      .get(
        `https://api.data.gov.sg/v1/environment/2-hour-weather-forecast${params}`,
      )
      .pipe(
        map((res) => {
          return res.data;
        }),
      )
      .pipe(
        catchError(() => {
          throw new ForbiddenException('Something went wrong');
        }),
      );
  }
}
