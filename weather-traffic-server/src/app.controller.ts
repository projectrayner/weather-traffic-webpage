import { Controller, Get, Query } from '@nestjs/common';
import { AppService } from './app.service';

@Controller('api')
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get('v1/traffic-images')
  getTrafficImages(@Query('date_time') date_time?: string) {
    return this.appService.getImages(date_time);
  }

  @Get('v1/weather-2hours')
  getWeather2Hour(@Query('date_time') date_time?: string) {
    return this.appService.getWeather2Hour(date_time);
  }
}
